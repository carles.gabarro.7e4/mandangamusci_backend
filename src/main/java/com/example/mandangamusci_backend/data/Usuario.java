package com.example.mandangamusci_backend.data;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Document(collection = "usuarios")
public class Usuario {
    @Id
    private String id;
    @Email(message = "Email no válido")
    @NotEmpty(message = "El email es obligatorio")
    private String email;

    @NotEmpty(message = "La contraseña es obligatoria")
    private String contrasena;

    private List<String> listaCanciones;
}
