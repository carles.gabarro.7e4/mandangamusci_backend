package com.example.mandangamusci_backend.data;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "canciones")
public class Cancion {

    @Id
    private String id;
    private String nombre;
    private String descripcion;
    private String imagen;
    private String audio;
}
