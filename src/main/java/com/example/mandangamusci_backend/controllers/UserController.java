package com.example.mandangamusci_backend.controllers;

import com.example.mandangamusci_backend.data.Usuario;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/usuarios")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public List<Usuario> getAllUsuarios() {
        return userRepository.findAll();
    }

    @PostMapping
    public Usuario createUsuario(@Valid @RequestBody Usuario usuario) {
        return userRepository.save(usuario);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Usuario> getUsuarioById(@PathVariable(value = "id") String userId) {
        Usuario usuario = userRepository.findById(userId).orElse(null);
        if (usuario == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(usuario);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Usuario> updateUsuario(@PathVariable(value = "id") String userId, @Valid @RequestBody Usuario usuarioDetails) {
        Usuario usuario = userRepository.findById(userId).orElse(null);
        if (usuario == null) {
            return ResponseEntity.notFound().build();
        }
        usuario.setEmail(usuarioDetails.getEmail());
        usuario.setContrasena(usuarioDetails.getContrasena());
        usuario.setListaCanciones(usuarioDetails.getListaCanciones());
        final Usuario updatedUsuario = userRepository.save(usuario);
        return ResponseEntity.ok(updatedUsuario);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUsuario(@PathVariable(value = "id") String userId) {
        Usuario usuario = userRepository.findById(userId).orElse(null);
        if (usuario == null) {
            return ResponseEntity.notFound().build();
        }
        userRepository.delete(usuario);
        return ResponseEntity.ok().build();
    }

}
