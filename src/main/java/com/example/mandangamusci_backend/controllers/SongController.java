package com.example.mandangamusci_backend.controllers;
import com.example.mandangamusci_backend.data.Cancion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
    @RestController
    @RequestMapping("/api/canciones")
    public class SongController {

        @Autowired
        private SongRepository songRepository;

        @GetMapping
        public List<Cancion> getAllCanciones() {
            return songRepository.findAll();
        }

        @PostMapping
        public Cancion createCancion(@RequestBody Cancion cancion) {
            return songRepository.save(cancion);
        }

        @GetMapping("/{id}")
        public ResponseEntity<Cancion> getCancionById(@PathVariable(value = "id") String cancionId) {
            Cancion cancion = songRepository.findById(cancionId).orElse(null);
            if (cancion == null) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().body(cancion);
        }

        @PutMapping("/{id}")
        public ResponseEntity<Cancion> updateCancion(@PathVariable(value = "id") String cancionId, @RequestBody Cancion cancionDetails) {
            Cancion cancion = songRepository.findById(cancionId).orElse(null);
            if (cancion == null) {
                return ResponseEntity.notFound().build();
            }
            cancion.setNombre(cancionDetails.getNombre());
            cancion.setDescripcion(cancionDetails.getDescripcion());
            cancion.setImagen(cancionDetails.getImagen());
            cancion.setAudio(cancionDetails.getAudio());
            final Cancion updatedCancion = songRepository.save(cancion);
            return ResponseEntity.ok(updatedCancion);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<Void> deleteCancion(@PathVariable(value = "id") String cancionId) {
            Cancion cancion = songRepository.findById(cancionId).orElse(null);
            if (cancion == null) {
                return ResponseEntity.notFound().build();
            }
            songRepository.delete(cancion);
            return ResponseEntity.ok().build();
        }
    }

