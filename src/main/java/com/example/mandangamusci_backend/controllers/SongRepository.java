package com.example.mandangamusci_backend.controllers;

import com.example.mandangamusci_backend.data.Cancion;
import com.example.mandangamusci_backend.data.Usuario;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface SongRepository extends MongoRepository<Cancion, String> {
}
