package com.example.mandangamusci_backend.controllers;

import com.example.mandangamusci_backend.data.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

public interface UserRepository extends MongoRepository<Usuario, String> {
}

